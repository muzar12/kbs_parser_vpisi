from pathlib import Path
import os
import urllib.request

# auto-py-to-exe program za exe delat
#za html use flask !!! https://pythonbasics.org/flask-upload-file/

seznam = []
path = os.getcwd()


def preberi(dat):
        f = open(dat, encoding="utf8")               #odpremo datoteko v utf8 formatu
        try:
            os.mkdir(path + "/izvoz")
            os.chdir(path + "/izvoz")
        except OSError:
            print("mapa ze ustvarjena.")
        os.chdir(path + "/izvoz")
        f.readline()                                 #prva vrstica se odstrani
        for line in f:
            oseba = []
            splitted = line.split(",")               #splitamo po ,
            for e in splitted[1:]:                   #gremo čez vse brez datuma vnosa in true
                oseba.append(e.strip("\""))          #odstranimo vse "" in shranimo v osebo
            oseba[2] = oseba[2].replace("-", ".")    #zamenjamo - z . v datumu rojstva
            oseba = oseba[:-1]
            if len(oseba) == 10:
                seznam.append(oseba)                    #osebo dodamo v seznam
            else:
                try:
                    int(oseba[4])
                    i = 0
                    nova = []
                    for asdf in oseba:
                        if i == 3:
                            nova.append(oseba[i] + oseba[i+1])
                        elif i == 4:
                            i += 1
                            continue
                        else:
                            nova.append(oseba[i])
                        i += 1
                    oseba = nova[:]
                except:
                    ...
                if len(oseba) == 10:
                    seznam.append(oseba)
                else:
                    i = 7
                    for asd in oseba[7:]:
                        if i == 7:
                            oseba[7] = oseba[7] + ", " + oseba[8]
                            i += 1
                        elif i == 8:
                            i += 1
                            continue
                        elif i == len(oseba)-1:
                            oseba[9] = oseba[len(oseba)-1]
                            i += 1
                        else:
                            oseba[8] = oseba[8] + ", " + asd
                            i += 1
                    seznam.append(oseba[:10])
        f.close()

        print("Uspešno prebrano. Počakajte da se okno zapre!")

def naredi_txt_file(oseba):
    f= open(oseba[0] + "_" + oseba[1] + ".txt", "w", encoding="utf-8")
    f.write("Potrdilo o vpisu\n" + "ime in priimek: " + oseba[0] + " " + oseba[1] + "\nDatum rojstva: " + oseba[2] +
            "\nNaslov: " + oseba[3] + "\nPošta in poštna številka: " + oseba[4] + " " + oseba[5] +
            "\nEmail Naslov: " + oseba[6] + "\nIme šole: " + oseba[7] + "\nSmer šolanja: "
            + oseba[8] + "\nSlika potrdila v ")
    if oseba[9].endswith(".pdf"):
        f.write("pdf formatu!\n")
    else:
        f.write("jpg formatu!\n")
    f.write("\nZ izpolnitvijo predmetne pristopne izjave se strinja, da tako Klub belokranjskih študentov, kot tudi vsi organi,"
            "\nki urejajo študentsko delovanje in v katere se klub v skladu s svojim krovnim aktom včlanjuje ter s katerimi sodeluje,"
            "\nza namen uresničevanja pravic in obveznosti v zvezi s članstvom v društvu in posredno v prej navedenih organih zbirajo,"
            "\nobdelujejo in uporabljajo moje osebne podatke skladno s Pravilnikom o varstvu osebnih podatkov in področno"
            "\nzakonodajo (Zakonom o varstvu osebnih podatkov (ZVOP-1, Uradni list RS, št. 94/07) in"
            "\nSplošno uredbo o varstvu podatkov (Uredba (EU) 2016/679)). Za osebne podatke v tem smislu se štejejo podatki, ki jih vsebuje"
            "\npristopna izjava in potrdilo o vpisu. Klub belokranjskih študentov se zavezuje,"
            "\nda bo z navedenimi osebnimi podatki ravnal skrbno, da jih bo varovali"
            "\nskladno z aktualnimi predpisi s področja varstva osebnih podatkov"
            "\nin da jih ne bo posredovali tretjim osebam brez izrecne privolitve člana"
            "\n*. Osebne podatke uporabljajo zgolj osebe, ki so za to pristojne, zlasti klubski aktivisti."
            "\nOsebne podatke hranimo zgolj za čas trajanja članstva v študentskem klubu, razen v izjemnih primerih."
            "\n*** Soglasje za posredovanje osebnih podatkov organom, ki urejajo študentsko delovanje in v katere se klub včlanjuje v skladu"
            "\ns svojim krovnim aktom ter s katerimi sodeluje, predstavlja izpolnitev predmetne pristopne izjave."
            "\n** Soglasje lahko član v kateremkoli trenutku pisno prekliče.")
    f.close()

def slika(oseba):
    if oseba[9].endswith(".pdf"):
        urllib.request.urlretrieve(oseba[9], oseba[0] + "_" + oseba[1] + ".pdf")
    else:
        urllib.request.urlretrieve(oseba[9], oseba[0] + "_" + oseba[1] + ".jpg")

def naredi_txt_oseb_za_zvitico(seznam):
    j = 1
    k = 0
    f = open("Osebe za zvitico.txt", "w", encoding="utf-8")
    f.write("OSEBE IN NASLOVI ZA ZVITICO\n")
    f.write(" |Ime in priimek: |Naslov:      |Pošta in poštna številka: |\n")
    for oseba in seznam:
        f.write(str(j) + ". ")
        f.write(oseba[0] + " " + oseba[1] + " | " + oseba[3] + " | " + oseba[4] + " " + oseba[5] + "\n")
        k += 1
        j += 1
    print("Uspešno ustvarjeno.")
    f.close()

preberi(input("Vnesi ime datoteke: "))  #preberemo datoteko
print("  za izdelavo oseb za zvitico pritisnite vnesite 1"
      "\n  za izdelavo vpisnih listov vnesite 2")
kaj = input("  št: ")
if kaj == 1:
    naredi_txt_oseb_za_zvitico(seznam)
if kaj == 2:
    i = 0
    while i < len(seznam):                   #nardimo txt file in prenesemo sliko
        naredi_txt_file(seznam[i])
        slika(seznam[i])
        i+=1
    print("Uspešno ustvarjeno!")
else:
    print("niste vnesli prave številke.")